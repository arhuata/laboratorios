﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intrcambio_numeros
{
    class Program
    {
        static void Main(string[] args)
        {
            int aux = 0;
            //intercambio de numeros
            Console.Write("Ingrese numero(1):");
            int num1 = int.Parse(Console.ReadLine());

            Console.Write("Ingrese numero(2):");
            int num2 = int.Parse(Console.ReadLine());
            aux = num1;
            num1 = num2;
            num2 = aux;
            Console.WriteLine($"los numeros cambiados son: num1={num1} y num2={num2}");
            Console.ReadKey();
        }
    }
}
