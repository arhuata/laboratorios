﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notas
{
    class Program
    {
        static double calcularPromedio(int n1,int n2,int n3)
        {
            return (n1 + n2 + n3) / 3;
        }
        static void Main(string[] args)
        {
            //calcula el promedio de 3 notas para un alumno
            Console.Write("Ingrese nota 1:");
            int nota1 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese nota 2:");
            int nota2 = int.Parse(Console.ReadLine());
            Console.Write("Ingrese nota 3:");
            int nota3 = int.Parse(Console.ReadLine());

            double promedio = calcularPromedio(nota1, nota2, nota3);
            if (promedio <= 10)
            {
                Console.WriteLine("DESAPROBADO");
            }
            else if (promedio <= 15)
            {
                Console.WriteLine("REGULAR");

            }
            else
            {
                Console.WriteLine("EXCELENTE");
            }

            Console.ReadKey();

        }
    }
}
