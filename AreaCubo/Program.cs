﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCubo
{
    class Program
    {
        static void Main(string[] args)
        {
            /*programa que calcula el volumen de un cubo
            declaracion de variables*/
            //1.ENTRADA
            int lado;
            int VolumenCubo;
            Console.Write("ingrese el lado del cubo");
            lado = int.Parse(Console.ReadLine());

            //2.PROCESO
            VolumenCubo = lado * lado * lado;

            
            //3.SALIDA
            //Console.WriteLine("el volumen del cubo de lado"+lado+"es:"+VolumenCubo);
            Console.WriteLine($"El volumen del cubo de lado {lado} es:{VolumenCubo}");
            Console.ReadKey();
        }
    }
}
