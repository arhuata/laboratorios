﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05distanciaEntrePuntos
{
    class Program
    {
        static int CalcularCuadradoLado(int a,int b)
        {
            return (b-a) * (b-a);
        }

        static void Main(string[] args)
        {
            //programa principal
            //dictancia(D)=sqrt(m2-n2
            Console.WriteLine("\nDatos de segundo lado");

            Console.WriteLine("ingrese el punto x1:");
            int x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese el punto x2:");
            int x2 = int.Parse(Console.ReadLine());
            //calcular cuadrado de lado
            int m2= CalcularCuadradoLado(x1, x2);
            Console.WriteLine("\nDatos de segundo lado");
            Console.WriteLine("ingrese el punto y1:");
            int y1 = int.Parse(Console.ReadLine());
            
            Console.WriteLine("ingrese el punto y2:");
            int y2 = int.Parse(Console.ReadLine());
            int n2 = CalcularCuadradoLado(y1, y2);
            //Console.WriteLine($"valor de m2:{m2}");
            //Console.WriteLine($"valor de n2:{n2}");
            double distancia = Math.Sqrt(m2 + n2);
            Console.WriteLine($"la distancia entre los puntos es:{distancia}");
            Console.ReadKey();
        }
    }
}
