﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04Areas
{
    class Program
    {
        static void titulo()
        {
            //Pograma que calcula las areas de figuras geometricas
            Console.WriteLine("Este programa calcula áreas geométricas");
            Console.WriteLine("========================================");
        }

        static void menu()
        {
            Console.WriteLine("1. Área de Cuadrado");
            Console.WriteLine("2. Área de Triángulo");
            Console.WriteLine("3. Área de Círculo");
        }
        static int elegirOpcion()
        {
            Console.Write(">>>Elija una opción <1-3>:  ");
            return int.Parse(Console.ReadLine());
        }

        static void evaluar(int opcion)
        {
            if (opcion < 0 || opcion > 3)
            {
                Console.WriteLine("Fin del programa...");
            }
            else
            {
                calcularAreas(opcion);

            }
        }

        static void calcularAreas(int opcion)
        {
            if (opcion == 1)
            {
                //Área cuadrado
                Console.Write("Ingrese lado del cuadrado: ");
                int ladoCuadrado = int.Parse(Console.ReadLine());

                int areaCuadrado = ladoCuadrado * ladoCuadrado;
                Console.WriteLine($"El área del cuadrado es: {areaCuadrado}");
            }
            else if (opcion == 2)
            {
                //Área Triangulo
                Console.Write("Ingrese base del triángulo: ");
                int baseTriangulo = int.Parse(Console.ReadLine());

                Console.Write("Ingrese altura del triángulo: ");
                int alturaTriangulo = int.Parse(Console.ReadLine());

                double areaTriangulo = baseTriangulo * alturaTriangulo / 2;
                Console.WriteLine($"El área del triángulo es: {areaTriangulo}");
            }
            else if (opcion == 3)
            {
                //Área Círculo
                const double PI = 3.141692;
                Console.Write("Ingrese radio del círculo: ");
                int radioCirculo = int.Parse(Console.ReadLine());

                double areaCirculo = PI * radioCirculo * radioCirculo;
                Console.WriteLine($"El área del círculo es: {areaCirculo}");
            }

        }
        static void Main(string[] args)
        {
            //LLamado a procedimientos
            titulo();
            menu();
            int opcion = elegirOpcion();
            evaluar(opcion);
            Console.ReadKey();
        }
    }
}



