﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02EcuacionCuadratica
{
    class Program
    {
        static void Main(string[] args)
        {
            /*obtener las raices de 
            x =(-b+sqrt(b*b-4*a*c))/(2*a)
            x =(-b-sqrt(b*b-4*a*c))/(2*a)
            */
            //1.ENTRADA
            int a, b, c;
            double x1, x2;
            Console.Write("ingrese valor de <a>:");
            a = int.Parse(Console.ReadLine());
            Console.Write("ingrese valor de <b>:");
            b = int.Parse(Console.ReadLine());

            Console.Write("ingrese valor de <c>:");
            c = int.Parse(Console.ReadLine());

            //2.PROCESO
            int subRaiz = b * b - 4 * a * c;
            if (subRaiz < 0)
            {
                Console.WriteLine("ERROR:numero imaginario.");
            }
            if (a == 0)
            {
                Console.WriteLine("ERROR!:<a> NO puede ser cero.");
            }
            else
            {
                x1 = (-b + Math.Sqrt(subRaiz)) / (2 * a);
                x2 = (-b - Math.Sqrt(subRaiz)) / (2 * a);
                Console.WriteLine($"la raiz x1={x1}");
                Console.WriteLine($"la raiz x2={x2}");
            }
            //3.SALIDA
            
            Console.ReadKey();

        }
    }
}
