﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03Tipodecambio
{
    class Program
    {
        static void Main(string[] args)
        {
            //programa que calcula el tipo de cambio de soles a dolares
            const double TIPO_CAMBIO = 3.5;
            Console.Write("ingrese la cantidad en soles:");
            int cantsoles = int.Parse(Console.ReadLine());
            double cantdolares = cantsoles / TIPO_CAMBIO;
            Console.WriteLine($"{cantsoles}nuevos soles en dolares es:{cantdolares}");
            Console.ReadKey();

        }
    }
}
